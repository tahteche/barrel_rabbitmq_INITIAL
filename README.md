barrel-rabbitmq [TRANSFERED TO: https://gitlab.com/tahteche/barrel_rabbitmq]
=====

An OTP application which publishes events in Barrel to RabbitMQ

Write Barrel Events to Console:
--------------------------------
After starting Barrel run the following command from the root of this repo:

1. rebar3 shell
2. When shell instance is started call `barrel_rabbitmq:print_changes(DB).` where `DB` is the database you will like to monitor. The Barrel instance is assumed to be `"http://localhost:7080/dbs/"`  
3. `CTRL + C` To stop montoring.

Build
-----

    $ rebar3 compile
