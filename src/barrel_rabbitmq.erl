%% Copyright 2017, Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(barrel_rabbitmq).
-author("Tah Teche").

-define(BARREL_URL,<<"http://localhost:7080/dbs/{db}">>).

%% API
-export([
	print_changes/1,
	stop_changes_listener/1
  ]).

print_changes(DB) ->
	DB_URL = make_db_url(DB),
	{ok, _} = application:ensure_all_started(barrel_httpc),
	{ok, Conn} = barrel_httpc:connect(DB_URL),

	Callback = fun changes_cb/1,

	ListenerOptions = #{since => 0, mode => sse, changes_cb => Callback },

	{ok, ListenerPid} = barrel_httpc:start_changes_listener(Conn, ListenerOptions).

	stop_changes_listener(ListenerPid) ->
		barrel_httpc:stop_changes_listener(ListenerPid).

%% Internal Functions

make_db_url(DB) ->
	DB1 = list_to_binary(DB),
	binary:replace(?BARREL_URL, <<"{db}">>, DB1).

changes_cb(Change) ->
		case maps:is_key(<<"deleted">>, Change) of
			true ->
				Change1 = Change#{<<"type">> => <<"delete">>},
				io:format("The following change just happened in the DB:~n~n~p~n~n",
							[Change1]);
			_ ->
				#{<<"rev">> := Rev} = Change,
				[Sequence, _] = binary:split(Rev, <<"-">>),
				case Sequence =:= <<"1">> of
					true ->
						Change1 = Change#{<<"type">> => <<"insert">>},
						io:format("The following change just happened in the DB:~n~n~p~n~n",
									[Change1]);
					false ->
						Change1 = Change#{<<"type">> => <<"update">>},
						io:format("The following change just happened in the DB:~n~n~p~n~n",
									[Change1])
				end
		end.