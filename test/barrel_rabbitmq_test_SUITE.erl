%% Copyright 2017, Tah Teche
%%
%% Licensed under the Apache License, Version 2.0 (the "License"); you may not
%% use this file except in compliance with the License. You may obtain a copy of
%% the License at
%%
%%   http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
%% WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
%% License for the specific language governing permissions and limitations under
%% the License.

-module(barrel_rabbitmq_test_SUITE).
-author("Tah Teche").


%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).

-export([
  print_changes/0,
  print_changes/1
]).

all() ->
  [
   print_changes
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(barrel_rabbitmq),
  Config.

init_per_testcase(_, Config) ->
  barrel_httpc:create_database(ct:get_config(db_url)),
  Config.

end_per_testcase(_, _Config) ->
  _ = barrel_httpc:delete_database(ct:get_config(db_url)),
  ok.

end_per_suite(Config) ->
  _ = application:stop(barrel_rabbitmq),
  Config.

print_changes()->
  [
    {require, db}
  ].

print_changes(_Config)->
  barrel_rabbitmq:print_changes(ct:get_config(db)),
  ok.